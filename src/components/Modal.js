import React, { PureComponent } from 'react';

class Modal extends PureComponent{
    render() {
        return(
            <div className="modal fade" tabIndex="-1" role="dialog" >
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Unable to retrieve photos</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.props.closeModal}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p>The required permission was not granted, please grant "view photos" permission</p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary" onClick={this.props.getPermission}>Grant</button>
                            <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.props.closeModal}>Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Modal;
