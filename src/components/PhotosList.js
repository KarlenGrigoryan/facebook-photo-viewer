import React, { PureComponent } from 'react';

class PhotosList extends PureComponent{
    render() {
        const { user } = this.props;

        return(
            <div className='col-lg-3 col-md-4 col-sm-6 my-1'>
                <img src={user.picture} alt="User picture" className="mx-auto my-auto d-block" />
            </div>
        )
    }
}

export default PhotosList;
