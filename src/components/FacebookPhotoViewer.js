/*global FB*/
import React, { Component } from 'react';
import ReactDOM from 'react-dom'

// Components
import FacebookAuthButton from './FacebookAuthButton';
import PhotosList from './PhotosList';
import Modal from './Modal';

class FacebookPhotoViewer extends Component{
    constructor() {
        super();
        this.state = {
            photos: null,
            auth: null
        }
    }

    loadFbLoginApi = () => {
        window.fbAsyncInit = () => {
            FB.init({
                appId      : "505748586642039",
                cookie     : true,  // enable cookies to allow the server to access
                // the session
                xfbml      : true,  // parse social plugins on this page
                version    : 'v2.1' // use version 2.1
            });
        };
        // Load the SDK asynchronously
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    };

    statusChangeCallback = (response) => {
        this.setState({
            auth: response
        });
        if (response && response.authResponse) {
            FB.api(
                `/${response.authResponse.userID}/photos`,
                'GET',
                {"fields":"picture","type":"uploaded"},
                (user) => {
                    this.setState({
                        photos: user.data
                    });
                    const modal = ReactDOM.findDOMNode(this.refs.modal);
                    modal.removeAttribute("style")
                }
            );
        }else if (this.state.auth && response.status === 'unknown') {
            const modal = ReactDOM.findDOMNode(this.refs.modal);
            modal.classList.add("show");
            modal.setAttribute("style", "display: block");
        }
    };

    checkLoginState = () => {
        FB.getLoginStatus((response) => {
            this.statusChangeCallback(response);
        });
    };

    handleFBLogin = () => {
        FB.login(this.checkLoginState,  {
            scope: 'user_photos',
            auth_type: 'rerequest'
        });
    };

    closeModal = () => {
        const modal = ReactDOM.findDOMNode(this.refs.modal);
        modal.removeAttribute("style")
    };

    componentDidMount() {
        this.loadFbLoginApi();
    }

    render() {
        const { auth, photos } = this.state;

        return (
            <>
                <h1>Facebook Photo Viewer</h1>
                <div className='container'>
                    {
                        !photos &&
                        <div>
                            <FacebookAuthButton handleFBLogin={this.handleFBLogin} />
                            <p className="lead">Please sign in with your Facebook account and make sure to grant "view photos" permission. Enjoy!</p>
                        </div>
                    }
                    {
                        (auth && auth.status === 'connected' && photos) &&
                            <div className="row align-items-center">
                            {
                                photos.map((photo, index) => {
                                    return <PhotosList user={photo} key={index} />
                                })
                            }
                        </div>
                    }
                </div>
                <Modal ref="modal" closeModal={this.closeModal} getPermission={this.handleFBLogin} />
            </>
        )

    }
}

export default FacebookPhotoViewer;
