import React, { PureComponent } from 'react';

class FacebookAuthButton extends PureComponent {
    render() {
        return(
            <button type='button' className='btn btn-primary mt-2' onClick={this.props.handleFBLogin}>Sign in with Facebook</button>
        )
    }
}

export default FacebookAuthButton;
