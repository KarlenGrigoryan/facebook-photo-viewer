import React from 'react';
import './App.css';

// Components
import FacebookPhotoViewer from './components/FacebookPhotoViewer';

function App() {
  return (
    <div className="App">
      <FacebookPhotoViewer />
    </div>
  );
}

export default App;
